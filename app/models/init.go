package models

import (
	"github.com/jinzhu/gorm"
)

// InitDB : Initializes DB and migrates changes
func InitDB() {
	db, err := gorm.Open(DBNAME, DBURL)
	db.LogMode(true)
	defer db.Close()
	if err != nil {
		panic(err)
	}

	db.AutoMigrate(&Template{})

}
