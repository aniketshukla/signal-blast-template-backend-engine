package models

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/rs/xid"
)

// Template struct is created in db
type Template struct {
	ID        uint      `gorm:"primary_key"`
	Subject   string    `gorm:"not null" json:"subject"`
	Body      string    `gorm:"not null" json:"body"`
	CreatedAt time.Time `grom:"not null" json:"created_at"`
	UpdatedAt time.Time `grom:"not null" json:"updated_at"`
	DB        *gorm.DB  `grom:"-" json:"-"`
	Error     string    `json:"error"`
	UniqueID  string    `json:"unique_id"`
	Name      string    `gorm:"not null" json:"name"`
}

// Connect issues a new connection to database
func (template *Template) Connect() {
	db, err := gorm.Open(DBNAME, DBURL)

	if err != nil {
		template.Error = err.Error()
		panic(err)
	}

	template.DB = db

}

// Close closes existing connection to database
func (template *Template) Close() {
	template.DB.Close()
}

// Create new template or adds to existing
func (template *Template) Create() {
	template.Connect()
	defer template.Close()
	template.UniqueID = xid.New().String()
	template.CreatedAt = time.Now().UTC()
	template.UpdatedAt = time.Now().UTC()

	if template.Name == "" {
		template.Name = template.UniqueID
	}

	template.DB.Create(&template)
}

//Sync exisitng template with backend
func (template *Template) Sync() {
	template.Connect()
	defer template.Close()

	// finding value
	templateSync := &Template{}
	template.DB.LogMode(true)
	template.DB.Where("unique_id = ?", template.UniqueID).First(templateSync)
	fmt.Println(templateSync)

	template.DB.Model(templateSync).Updates(
		Template{
			Body:      template.Body,
			Subject:   template.Subject,
			Name:      template.Name,
			UpdatedAt: time.Now().UTC(),
		},
	)

}

// GetAll method returns all available template
func (template *Template) GetAll() (templateReturn []Template) {
	template.Connect()
	defer template.Close()

	template.DB.Find(&templateReturn)
	return

}

// GetByID method return information about specific id
func (template *Template) GetByID(id string) {
	template.Connect()
	defer template.Close()
	template.DB.Where("unique_id = ?", id).First(template)
}
