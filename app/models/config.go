package models

import (
	// db init
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

//DBNAME attach supported db only
var DBNAME = "sqlite3"

//DBURL path to db
var DBURL = "/tmp/appdb"
