package controllers

import (
	"signal-blast-template-backend-engine/app/models"

	"github.com/revel/revel"
)

//Template details
type Template struct {
	*revel.Controller
}

//Add controller for adding data
func (c Template) Add() revel.Result {
	template := &models.Template{}
	c.Params.BindJSON(template)
	template.Create()
	return c.RenderJSON(template)
}

//Sync controller for updating data
func (c Template) Sync() revel.Result {
	template := &models.Template{}
	c.Params.BindJSON(template)
	template.Sync()
	return c.RenderJSON(template)
}

//ListAll controller to list all templates
func (c Template) ListAll() revel.Result {
	template := new(models.Template)
	templateList := template.GetAll()
	return c.RenderJSON(templateList)
}

// GetByID gets template by id
func (c Template) GetByID(id string) revel.Result {
	template := new(models.Template)
	template.GetByID(id)
	return c.RenderJSON(template)
}
